var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitecujlgg/collections/";
var mLabAPIKey = "apiKey=E7qNG698tg90u0NLfeSbApQJH5mWRJmz";
var requestJson = require('request-json');

const enigma = require('enigma-code');
const valorEncriptacion = 10;//puede ser cualquier numero
let key = 'jose2018';
let mypassword = 'jose12345678';

app.listen(port);
console.log("API escuchando en el puerto" + port);

app.get('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");

    res.send(
      {
        "msg" : "Bienvenido a la API de Tech University"
      }
    )
  }
);

// Consulta de datos un cliente //

app.get('/apitechu/v1/clientes/:id',
  function(req, res) {
    console.log("GET /apitechu/v1/clientes");

    var id = req.params.id;
    var query = 'q={"id" : ' + id + '}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("clientes?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
        response = {
          "msg" : "Error obteniendo cliente."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          response = body[0];
        } else {
          response = {
            "msg" : "Cliente no encontrado."
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}
);

// Login de un cliente //
// Se recuperan los datos del cliente//
// Se compara la password recuperada de BBDD y //
// la password indicada por el cliente //

app.post("/apitechu/v1/login",
  function(req, res) {
    console.log("POST /apitechu/v1/login");

    var email = req.body.email;
    var password = req.body.password;

    var query = 'q={"email":"' + email + '"}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("clientes?" + query + "&" + mLabAPIKey,
      function(errLoggin, resMLabLoggin, bodyLoggin) {
        if (errLoggin) {
          response = {
            "msg" : "Error obteniendo clientes."
          }
          res.status(500);
          res.send(response);
        } else {
          if (bodyLoggin.length > 0) {

            let hash = bodyLoggin[0].password;

            enigma.comparar(hash,password, function(err, rescom) {
		            if(err) {
                  response ={
                    "msg":"Error Loggin"
                  };
                  res.status(500);
                  res.send(response);
                } else {

                    if (rescom) {

                      var queryPut='q={"id":'+bodyLoggin[0].id+'}';
                      var putBody ='{"$set":{"logged":true}}';
                      response = bodyLoggin[0];

                      httpClient.put("clientes?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
                        function(errPut,resMlabPut,bodyPut){
                          console.log('ERROR PUT:'+errPut);
                          console.log('BODY PUT:'+bodyPut);
                          res.send(response);
                        }
                      );
                    } else {
                        response = {
                          "msg" : "Password no valida"
                        };
                        res.status(500);
                        res.send(response);
                      }
                  }
              }
            )
          } else {
              response = {
                "msg" : "Cliente no encontrado."
              };
              res.status(404);
              res.send(response);
            }
        }
      }
    )
  }
);

// Logout de un cliente //

app.post("/apitechu/v1/logout",
  function(req, res) {
    console.log("POST /apitechu/v1/logout");
    console.log("email is: " + req.body.email);

    var email = req.body.email;
    var query = 'q={"email":"' + email + '", "logged":true}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("clientes?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo usuario."
          }
          res.status(500);
          res.send(response);

        } else {
          if (body.length > 0) {

            var queryPut='q={"id":'+body[0].id+'}';
            var putBody ='{"$unset":{"logged":""}}';
            response = body[0];

            httpClient.put("clientes?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
              function(errPut,resMlabPut,bodyPut){
                console.log('ERROR PUT:'+errPut);
                console.log('BODY PUT:'+bodyPut);
                res.send(response);
              }
            )
          } else {
              response = {
                "msg" : "Usuario no encontrado."
              };
              res.status(404);
              res.send(response);
            }
        }
      }
    )
  }
);

// Alta de un nuevo cliente //
// Se valida que el cliente no existe //
// Se asigna un nuevo ID, consecutivo al mayor de la  BBDD//
// Se encripta la password y se da de alta el nuevo cliente //

app.post("/apitechu/v1/altacliente",
  function(req, res) {
    console.log("POST /apitechu/v1/altacliente");

    var email = req.body.email;
    var password = req.body.password;
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;

    var querysort='s={"id":-1}';
    var queryvalidate='q={"email":"' + email + '"}';
    var querymaxid='q={}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("clientes?"+ queryvalidate + "&" + mLabAPIKey,
      function(errvuser, resMLabvuser, bodyvuser) {
        if (errvuser) {
          response = {
            "msg" : "Error, el usuario existe"
          };
          res.status(500);
          res.send(response);

        } else {
            if (bodyvuser.length > 0) {
              response = {
                "msg" : "Error, el usuario existe"
              }
              res.status(500);
              res.send(response);
            } else {

                httpClient.get("clientes?"+ querymaxid + "&" + querysort + "&" + 'l=1' + "&" + mLabAPIKey,
                  function(errmaxid, resMLabmaxid, bodymaxid) {

                    if (errmaxid) {
                      response = {
                        "msg" : "Error insertando usuario."
                      };
                      res.status(500);
                      res.send(response);

                    } else {

                        if (bodymaxid.length>0) {

                          var newId = bodymaxid[0].id + 1;

                          var passwordEncriptada = enigma.genHash(valorEncriptacion,key,password,function(err,hash){
                                                    if(err) return console.log(err);
                                                    return(hash);
                          });

                          var insertBody = '{"id": '+ newId +' , "first_name":"' + firstname + '", "last_name":"' + lastname + '", "email":"' + email + '", "password":"' + passwordEncriptada + '", "logged":true}';
                          console.log("insertbody: " + insertBody);

                          httpClient.post("clientes?" + mLabAPIKey, JSON.parse(insertBody),
                            function(errPost, resMLabPost, bodyPost) {
                              response = insertBody;
                              res.send(response);
                            }
                          )
                        } else {
                            response = {
                              "msg" : "Error recuperando max. id."
                            };
                            res.status(500);
                            res.send(response);
                          }
                      }
                  }
                )
              }
          }
      }
    )
  }
);

// Se declaran las funciones de encriptado y comparación de claves //

enigma.genHash(valorEncriptacion, key, mypassword, function(err, hash){
	if(err) return console.log(err);
	console.log('hash', hash);

	enigma.comparar(hash, mypassword, function(err, response){
		if(err) return console.log(err);
		if(response) return console.log("Test passed");
		return console.error("Test not passed");
	})
});

// Consulta de las cuentas de un cliente
// Se recive por parametro el id del cliente y se recuperan todas sus cuentas

app.get('/apitechu/v1/clientes/:id/cuentas',
  function(req, res) {
    console.log("GET /apitechu/v1/clientes/:id/cuentas");

    var clienteid = req.params.id;
    var query = 'q={"clienteid" : ' + clienteid + '}';
    var querysort='s={"iban":1}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("cuentas?"+ query + "&" + querysort + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo IBAN."
          }
          res.status(500);
        } else {
            if (body.length > 0) {
              response = body;
            } else {
                response = {
                  "msg" : "IBAN no encontrado."
                };
                res.status(404);
              }
          }
        res.send(response);
      }
    )
  }
);

// Crear una cuentas de un cliente
// Se recupera el max.id de cuentas
// Componemos el iban en funcion de fecha y hora
// POST de la nueva cuentas
//

app.post('/apitechu/v1/clientes/altacuenta',
  function(req, res) {
    console.log("POST /apitechu/v1/clientes/altacuenta");

    var querymaxcuentaid='q={}';
    var querysort = 's={"cuentaid":-1}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("cuentas?"+ querymaxcuentaid + "&" + querysort + "&" + 'l=1' + "&" + mLabAPIKey,
      function(errmaxcuentaid, resMLabmaxcuentaid, bodymaxcuentaid) {

        if (errmaxcuentaid) {
          response = {
            "msg" : "Error insertando cuenta."
          };
          res.status(500);
          res.send(response);
        } else {
            if (bodymaxcuentaid.length>0) {

              var clienteid = req.body.clienteid;

              var date = new Date();
              var d = date.getDate();
              var dia = (d < 10) ? '0' + d : d;
              var m = date.getMonth() + 1;
              var mes = (m < 10) ? '0' + m : m;
              var h = date.getHours();
              var hora = (h < 10) ? '0' + h : h;
              var mto = date.getMinutes();
              var minuto = (mto < 10) ? '0' + mto : mto;

              var newiban = "BA01 0001 7999 " + date.getFullYear() + " " + mes + dia + " " + hora + minuto;

              var newCuentaId = bodymaxcuentaid[0].cuentaid + 1;
              var insertCuenta = '{"iban":"' + newiban + '", "cuentaid":' + newCuentaId + ', "clienteid":' + clienteid + ', "saldo":0}';

              httpClient.post("cuentas?" + mLabAPIKey, JSON.parse(insertCuenta),
                function(errPostCuenta, resMLabPostCuenta, bodyPostCuenta) {

                  response = insertCuenta;
                  res.send(response);
                }
              )
            }
          }
      }
    )
  }
);

// Consulta de los movimientos de una cuenta de un cliente

app.get('/apitechu/v1/cuentas/:cuentaid/movimientos',
  function(req, res) {
    console.log("GET /apitechu/v1/cuentas/:cuentaid/movimientos");

    var cuentaid = req.params.cuentaid;
    var query = 'q={"cuentaid" : ' + cuentaid + '}';
    var querysort='s={"fecha":-1}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("movimientos?"+ query + "&" + querysort + "&" + mLabAPIKey,
      function(errmov, resMLabmov, bodymov) {
        if (errmov) {
          response = {
            "msg" : "Error obteniendo IBAN."
          }
          res.status(500);
        } else {
            if (bodymov.length > 0) {

              response = bodymov;
            } else {
                response = {
                  "msg" : "IBAN no encontrado."
                };
                res.status(404);
              }
          }
        res.send(response);
      }
    )
  }
);

// Alta de movimientos de una cuenta de entrada
// Validamos que existe la cuenta indicada
// Recuperamos el max. id de movimientos
// Recuperamos fecha y hora del sistema
// POST del nuevo movimiento
// Actualizamos saldo de la cuenta
// Si la actualización del saldo de la cuenta da error eliminamos
// el movimiento que se había insertado

app.post("/apitechu/v1/cuentas/altamovimiento",
  function(req, res) {
    console.log("POST /apitechu/v1/cuentas/altamovimiento");

    var cuentaid = req.body.cuentaid;
    var tipoAltaMov = req.body.tipo;
    var importeAltaMov = req.body.importe;
    var observacionesAltaMov = req.body.observaciones;

    var dateAltaMov = new Date();
    var dAltaMov = dateAltaMov.getDate();
    var diaAltaMov = (dAltaMov < 10) ? '0' + dAltaMov : dAltaMov;
    var mAltaMov = dateAltaMov.getMonth() + 1;
    var mesAltaMov = (mAltaMov < 10) ? '0' + mAltaMov : mAltaMov;
    var hAltaMov = dateAltaMov.getHours();
    var hAltaMov = (hAltaMov < 10) ? '0' + hAltaMov : hAltaMov;
    var mAltaMov = dateAltaMov.getMinutes();
    var minutoAltaMov = (mAltaMov < 10) ? '0' + mAltaMov : mAltaMov;
    var fechaAltaMov = diaAltaMov + "/" + mesAltaMov + "/" + dateAltaMov.getFullYear();
    var horaAltaMov = hAltaMov + ":" + minutoAltaMov;

    var querysort='s={"movimientoid":-1}';
    var queryvalidate='q={"cuentaid":' + cuentaid + '}';
    var querymaxid='q={}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("cuentas?"+ queryvalidate + "&" + mLabAPIKey,
      function(errvcuenta, resMLabvcuenta, bodyvcuenta) {
        if (errvcuenta) {
            response = {
              "msg" : "Error Alta Movimiento"
            };
            res.status(500);
            res.send(response);
        } else {
            if (bodyvcuenta.length > 0) {

              httpClient.get("movimientos?"+ querymaxid + "&" + querysort + "&" + 'l=1' + "&" + mLabAPIKey,
                function(errmaxid, resMLabmaxid, bodymaxid) {

                  if (errmaxid) {
                    response = {
                      "msg" : "Error Alta Movimiento"
                    };
                    res.status(500);
                    res.send(response);
                  } else {

                      if (bodymaxid.length>0) {
                        var newMovimientoId = bodymaxid[0].movimientoid + 1;

                        if (tipoAltaMov == "Ingreso Efectivo") {
                          var saldoposAltaMov = bodyvcuenta[0].saldo + parseFloat(importeAltaMov);
                        } else {
                          var saldoposAltaMov = bodyvcuenta[0].saldo - importeAltaMov
                          };

                        var saldoantAltaMov = bodyvcuenta[0].saldo;

                        var insertBodyMov = '{"cuentaid": ' + cuentaid + ', "movimientoid": ' + newMovimientoId + ', "importe":' + importeAltaMov + ', "saldoant":' + saldoantAltaMov + ', "saldopos":' + saldoposAltaMov + ', "tipo":"' + tipoAltaMov + '", "fecha":"' + fechaAltaMov + '", "hora":"' + horaAltaMov + '", "observaciones":"' + observacionesAltaMov + '"}';

                        httpClient.post("movimientos?" + mLabAPIKey, JSON.parse(insertBodyMov),
                          function(errPostMov, resMLabPostMov, bodyPostMov) {
                            console.log("Respuesta de Insert Movimiento");
                            //console.log("Mlab id mov: " + bodyPostMov.this.$oid);

                            if (errPostMov) {
                              response =  {
                                "msg":"Error Alta Movimiento"
                              };
                              res.status(500);
                              res.send(response);
                            } else {

                  // Actualizar saldo cuenta

                              var queryPutCuentas='q={"cuentaid":'+bodyvcuenta[0].cuentaid+'}';
                              var putBodyCuentas ='{"$set":{"saldo":'+saldoposAltaMov+'}}';

                              httpClient.put("cuentas?"+queryPutCuentas+'&'+mLabAPIKey, JSON.parse(putBodyCuentas),
                                function(errPutCuentas,resMlabPutCuentas,bodyPutCuentas){
                                  if (errPutCuentas) {

                  //Si la actualizacon del saldo de la cuenta da error: Borramos el Movimiento

                                    var queryPutMovimientos='q={"movimientosid":'+newMovimientoId+'}';
                                    var putBodyMovimientos ='{"$set":{}}';

                                    httpClient.put("movimientos?"+queryPutMovimientos+'&'+mLabAPIKey, JSON.parse(putBodyMovimientos),
                                      function(errPutCuentas,resMlabPutCuentas,bodyPutCuentas){
                                        response = {
                                          "msg":"Error Alta Movimiento"
                                        };
                                        res.status(500);
                                        res.send(Response);
                                      });
                                  } else {
                                      console.log('Finaliza proceso Alta Movimiento');

                                      response = {
                                        "msg":"Proceso totalmente finalizado"
                                      };
                                      res.send(response);
                                    }
                                }
                              );
                            }
                          }
                        )
                      } else {
                          response = {
                            "msg" : "Error Alta Movimiento"
                          };
                          res.status(500);
                          res.send(response);
                        }
                    }
                }
              )

            } else {
                response = {
                  "msg" : "Error Alta Movimiento"
                }
                res.status(500);
                res.send(response);
              }
          }
      }
    )
  }
);
